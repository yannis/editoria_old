module.exports = {
  secret: 'EXAMPLEDONTUSE',
  API_ENDPOINT: '/api',
  routes: 'app/routes.jsx',
  navigation: 'app/components/Navigation/Navigation.jsx',
  theme: 'EditoriaTheme',
  authsome: {
    mode: 'editoria',
    teams: {
      teamProduction: {
        name: 'Production Editor',
        permissions: 'all'
      },
      teamCopyEditor: {
        name: 'Copy Editor',
        permissions: 'update'
      },
      teamauthors: {
        name: 'Author',
        permissions: 'update'
      }
    }
  }
}
