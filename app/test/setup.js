import jsdom from 'jsdom'
import Module from 'module'
import { resolve } from 'path'

global.document = jsdom.jsdom('<!doctype html><html><body></body></html>')
global.window = document.defaultView
global.navigator = { userAgent: 'node.js' }

/**
 * Monkey-patching native require, because Webpack supports requiring files, other
 * than JavaScript. But Node doesn't recognize them, so they should be ignored.
 * IMPORTANT: don't use arrow functions because they change the scope of 'this'!
 * https://medium.com/@TomazZaman/how-to-get-fast-unit-tests-with-out-webpack-793c408a076f#.gq5vdquyb
 */
Module.prototype.require = function (path) {
  // TODO - could possibly replace these two lines with prunk
  const types = /\.(s?css|sass|less|svg|html|png|jpe?g|gif)$/
  if (path.search(types) !== -1) return {}

  // Mimics Webpack's "alias" feature
  if (path === 'editor') {
    path = resolve('app/registry/Substance/Writer.jsx')
  }

  return Module._load(path, this)
}
