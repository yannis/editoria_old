import React from 'react'
import { Route } from 'react-router'

import { requireAuthentication } from 'pubsweet-core/app/components/AuthenticatedComponent'

// Manage
import Manage from 'pubsweet-core/app/components/Manage/Manage'
import UsersManager from 'pubsweet-core/app/components/UsersManager/UsersManager'
import TeamsManager from 'pubsweet-core/app/components/TeamsManager/TeamsManager'

// Public
import Blog from 'pubsweet-core/app/components/Blog/Blog'
import ScienceReader from 'pubsweet-core/app/components/ScienceReader/ScienceReader'

// Authentication
import Login from 'pubsweet-core/app/components/Login/Login'
import Signup from 'pubsweet-core/app/components/Signup/Signup'

// Book Builder
import BookList from './components/BookBuilder/BookList'
import BookBuilder from './components/BookBuilder/BookBuilder'
import SimpleEditorWrapper from './components/SimpleEditor/SimpleEditorWrapper'

export default (
  <Route>
    <Route path="/" component={Blog} />

    <Route path="/manage" component={requireAuthentication(Manage, 'read', (state) => state.collections[0])}>
      <Route path="books" component={BookList} />

      <Route path="books/:id/book-builder" component={BookBuilder} />
      <Route path="books/:bookId/fragments/:fragmentId" component={SimpleEditorWrapper} />

      <Route path="users" component={UsersManager} />
      <Route path="teams" component={TeamsManager} />
    </Route>

    <Route path="/login" component={Login} />
    <Route path="/signup" component={Signup} />
    <Route path="/:id" component={ScienceReader} />
  </Route>
)
