import React from 'react'
import { shallow } from 'enzyme'
import expect from 'expect.js'
import sinon from 'sinon'

import { Division } from '../Division'
import Chapter from '../Chapter'

let props = {
  title: 'Some Division',
  chapters: [
    { title: 'A chapter', index: 0 },
    { title: 'Another chapter', index: 1 }
  ],
  type: 'front',
  outerContainer: {},
  add: sinon.spy(),
  remove: sinon.spy(),
  update: sinon.spy(),
  roles: []
}

describe('Division', () => {
  it('should render the title', () => {
    const division = shallow(<Division {...props}/>)
    const header = division.find('h1')
    const title = header.text().trim()
    expect(title).to.equal('Some Division')
  })

  it('should have an add button', () => {
    const division = shallow(<Division {...props} />)
    const button = division.find('a')
    expect(button).not.to.be(undefined)
    const buttonText = button.text().trim()
    expect(buttonText).to.equal('add component')
  })

  it('should change the add button text depending on the division type', () => {
    props.type = 'body'
    const division = shallow(<Division {...props} />)
    const button = division.find('a')
    expect(button).not.to.be(undefined)
    const buttonText = button.text().trim()
    expect(buttonText).to.equal('add chapter')
    props.type = 'front'
  })

  it('should render a list of chapters', () => {
    const division = shallow(<Division {...props} />)
    const chapters = division.find(Chapter).nodes
    expect(chapters.length).to.equal(2)
  })

  it('it should render a message if no chapters are found', () => {
    props.chapters = []

    const division = shallow(<Division {...props} />)
    const container = division.find('#displayed')
    const message = container.text().trim()

    expect(message).to.equal('There are no components in this division.')

    props.chapters = [
      { title: 'A chapter', index: 0 },
      { title: 'Another chapter', index: 1 }
    ]
  })

  it('should call the add prop function when the add button is clicked', () => {
    const sandbox = sinon.sandbox.create()
    const spy = sandbox.spy(Division.prototype, '_onAddClick')
    const division = shallow(<Division {...props} />)

    expect(spy.callCount).to.be(0)
    expect(props.add.callCount).to.be(0)

    // has the _onAddClick function been called
    const button = division.find('a')
    button.simulate('click')
    expect(spy.callCount).to.be(1)

    // has the add property function been called
    // (which will call the redux createFragment action)
    expect(props.add.callCount).to.be(1)

    sandbox.restore()
  })

  it('should call the remove and update props if a chapter is deleted', () => {
    const division = shallow(<Division {...props} />)

    expect(props.remove.callCount).to.be(0)
    expect(props.update.callCount).to.be(0)

    const instance = division.instance()
    instance._onRemove(props.chapters[0])

    // remove the first, which will trigger an update on the second's position
    expect(props.remove.callCount).to.be(1)
    expect(props.update.callCount).to.be(1)

    props.remove.reset()
    props.update.reset()

    props.chapters = [
      { title: 'A chapter', index: 0 },
      { title: 'Another chapter', index: 1 }
    ]
  })

  it('should call the update prop on reorder of chapters', () => {
    const division = shallow(<Division {...props} />)
    const instance = division.instance()

    expect(props.update.callCount).to.be(0)
    instance._onMove(1, 0)
    expect(props.update.callCount).to.be(2)
  })
})
