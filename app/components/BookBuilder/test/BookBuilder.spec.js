import React from 'react'
import { shallow } from 'enzyme'
import expect from 'expect.js'
import { every } from 'lodash'
import sinon from 'sinon'

import { BookBuilder } from '../BookBuilder'
import Division from '../Division'

let props = {
  book: { title: 'Pride and Prejudice' },
  chapters: [
    { title: 'Chapter One', division: 'front' },
    { title: 'Chapter Two', division: 'front' },
    { title: 'Chapter Three', division: 'body' },
    { title: 'Chapter Four', division: 'body' },
    { title: 'Chapter Five', division: 'back' },
    { title: 'Chapter Six', division: 'back' }
  ],
  actions: {
    createFragment: sinon.spy()
  },
  userRoles: []
}

function getDivisions () {
  const bookBuilder = shallow(<BookBuilder {...props} />)
  return bookBuilder.find(Division).nodes
}

describe('BookBuilder', () => {
  it('should render 3 division components', () => {
    const divisions = getDivisions()
    expect(divisions.length).to.equal(3)
  })

  it('the first division should be called front matter', () => {
    const firstDivision = getDivisions()[0]
    const firstName = firstDivision.props.title

    expect(firstName).to.equal('Front Matter')
  })

  it('the first division should only contain front matter components', () => {
    const firstDivision = getDivisions()[0]

    const firstChapters = firstDivision.props.chapters
    expect(firstChapters.length).to.equal(2)

    const correctDivisions = every(firstChapters, { division: 'front' })
    expect(correctDivisions).to.be(true)
  })

  it('the second division should be called body', () => {
    const secondDivision = getDivisions()[1]
    const secondName = secondDivision.props.title

    expect(secondName).to.equal('Body')
  })

  it('the second division should only contain body chapters', () => {
    const secondDivision = getDivisions()[1]

    const secondChapters = secondDivision.props.chapters
    expect(secondChapters.length).to.equal(2)

    const correctDivisions = every(secondChapters, { division: 'body' })
    expect(correctDivisions).to.be(true)
  })

  it('the third division should be called back matter', () => {
    const thirdDivision = getDivisions()[2]
    const thirdName = thirdDivision.props.title

    expect(thirdName).to.equal('Back Matter')
  })

  it('the third division should only contain back matter components', () => {
    const thirdDivision = getDivisions()[2]

    const thirdChapters = thirdDivision.props.chapters
    expect(thirdChapters.length).to.equal(2)

    const correctDivisions = every(thirdChapters, { division: 'back' })
    expect(correctDivisions).to.be(true)
  })
})
