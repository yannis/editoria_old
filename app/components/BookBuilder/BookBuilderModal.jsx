import React from 'react'
import { Modal } from 'react-bootstrap'
import TeamManager from './TeamManager/TeamManager'

export class BookBuilderModal extends React.Component {
  render () {
    const {
      title,
      show,
      toggle,
      successText,
      successAction,
      container,
      chapter,
      type,
      action,
      size,
      teams,
      users,
      updateTeam
    } = this.props

    const modalSize = size || null
    let modalBodyText = ''

    const success = successAction ? <a className="modal-button add-chapter bb-modal-act"
      onClick={successAction}>
      { successText }
    </a> : null

    if (action === 'delete') {
      modalBodyText = (
        <div>
          Are you sure you want to delete { type } "{ chapter.title }"?
        </div>
      )
    } else if (action === 'unlock') {
      modalBodyText = (
        <div>
          This action will unlock the chapter that
          is currently being edited. <br />
          Use with caution.
        </div>
      )
    } else if (action === 'EditoriaTeamManager') {
      modalBodyText = (
        <div>
          <TeamManager
            teams={teams}
            users={users}
            updateTeam={updateTeam}
         />
        </div>
      )
    } else if (action === 'workflow-warning') {
      modalBodyText = (
        <div>
          This action will revoke your access to this fragment.
          <br />
          Are you sure you want to continue?
        </div>
      )
    }

    return (
      <Modal
        show={show}
        onHide={toggle}
        container={container}
        className="modal"
        bsSize={modalSize}
      >

        <Modal.Header>
          <Modal.Title>
            { title }
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          { modalBodyText }
        </Modal.Body>

        <Modal.Footer>
          <div className="buttons-container">

            <a className="modal-button discard-chapter bb-modal-cancel"
              onClick={toggle}>
              Cancel
            </a>

            {success}

          </div>
        </Modal.Footer>

      </Modal>
    )
  }
}

BookBuilderModal.propTypes = {
  chapter: React.PropTypes.object,
  title: React.PropTypes.string.isRequired,
  action: React.PropTypes.string.isRequired,
  type: React.PropTypes.string,
  successText: React.PropTypes.string,
  successAction: React.PropTypes.func,
  show: React.PropTypes.bool.isRequired,
  toggle: React.PropTypes.func.isRequired,
  container: React.PropTypes.object.isRequired,
  size: React.PropTypes.string,
  teams: React.PropTypes.array,
  users: React.PropTypes.array,
  updateTeam: React.PropTypes.func
}

export default BookBuilderModal
