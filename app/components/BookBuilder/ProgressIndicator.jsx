import React from 'react'
import { includes } from 'lodash'
import { Alert } from 'react-bootstrap'
import BookBuilderModal from './BookBuilderModal'
import styles from './styles/bookBuilder.local.scss'

export class ProgressIndicator extends React.Component {
  constructor (props) {
    super(props)

    this._isAllowedToChange = this._isAllowedToChange.bind(this)
    this._onClick = this._onClick.bind(this)
    this._toggleModal = this._toggleModal.bind(this)
    this._changeWorkflowState = this._changeWorkflowState.bind(this)

    this.progressValues = {
      style: ['To Style', 'Styling', 'Styled'],
      edit: ['To Edit', 'Editing', 'Edited'],
      review: ['To Review', 'Reviewing', 'Reviewed'],
      clean: ['To Clean', 'Cleaning', 'Cleaned']
    }

    this.state = {
      showWarning: false,
      showError: false
    }
  }

  _isAllowedToChange () {
    const { type, roles, chapter } = this.props

    if (includes(roles, 'production-editor')) return true

    const isOne = (chapter.progress[type] === 1)

    if (chapter.progress[type] === 1 && type === 'edit' && includes(roles, 'copy-editor')) return true
    if (isOne && type === 'review' && includes(roles, 'author')) return true

    return false
  }

  _onClick () {
    const { roles } = this.props
    if (!this._isAllowedToChange()) {
      this.setState({ showError: true })
      setTimeout(() => { this.setState({showError: false}) }, 3000)
      return
    } else if (includes(roles, 'production-editor')) {
      this._changeWorkflowState()
    } else {
      this._toggleModal()
    }
  }

  _toggleModal () {
    this.setState({ showWarning: !this.state.showWarning })
  }

  _changeWorkflowState () {
    const { book, chapter, update, type, viewOrEdit } = this.props
    const { progressValues } = this

    const list = progressValues[type]

    let position = chapter.progress[type]
    position += 1     // move up a level

    if (position >= list.length) {
      position = 0    // or cycle back to the beginning
    }

    chapter.progress[type] = position

    update(book, chapter)
    this.setState({ showWarning: false })
    viewOrEdit()
  }

  render () {
    const { type, chapter, hasIcon, outerContainer } = this.props
    const { progressValues } = this

    let ErrorMsg = this.state.showError
    ? (
      <Alert bsStyle="warning" className={styles.noWritesError}>
        You don't have access to perfom this action. Please contact your Production Editor.
      </Alert>
    )
    : null

    let icon = ''
    if (hasIcon) {
      icon = <i className="fa fa-angle-right" />
    }

    const warningModal = (
      <BookBuilderModal
        title="Change of workflow status"
        action="workflow-warning"
        successText="OK"
        successAction={this._changeWorkflowState}
        show={this.state.showWarning}
        toggle={this._toggleModal}
        container={outerContainer}
      />
    )

    return (
      <span>
        { ErrorMsg }
        <li className={'progress' + chapter.progress[type]}
          onClick={this._onClick}>

          { progressValues[type][chapter.progress[type]] } &nbsp;
          { icon }
          { warningModal }

        </li>
      </span>
    )
  }
}

ProgressIndicator.propTypes = {
  type: React.PropTypes.string.isRequired,
  book: React.PropTypes.object.isRequired,
  chapter: React.PropTypes.object.isRequired,
  hasIcon: React.PropTypes.bool,
  update: React.PropTypes.func.isRequired,
  roles: React.PropTypes.array.isRequired,
  outerContainer: React.PropTypes.object.isRequired,
  viewOrEdit: React.PropTypes.func
}

export default ProgressIndicator
