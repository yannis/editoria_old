import React from 'react'
import { DropdownButton, MenuItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { DragSource, DropTarget } from 'react-dnd'
import { findDOMNode } from 'react-dom'

import { includes, get, map, flow, slice } from 'lodash'

import BookBuilderModal from './BookBuilderModal'
import ProgressIndicator from './ProgressIndicator'
import TextInput from 'pubsweet-core/app/components/PostsManager/TextInput'
import styles from './styles/bookBuilder.local.scss'

const itemTypes = {
  CHAPTER: 'chapter'
}

const chapterSource = {
  beginDrag (props) {
    return {
      id: props.id,
      no: props.no,
      division: props.chapter.division
    }
  },

  isDragging (props, monitor) {
    return props.id === monitor.getItem().id
  }
}

const chapterTarget = {
  // for an explanation of how this works go to
  // https://github.com/gaearon/react-dnd/blob/master/examples/04%20Sortable/Simple/Card.js

  hover (props, monitor, component) {
    // can only reorder within the same division
    const dragDivision = monitor.getItem().division
    const hoverDivision = props.chapter.division

    if (dragDivision !== hoverDivision) { return }

    const dragIndex = monitor.getItem().no
    const hoverIndex = props.no

    if (dragIndex === hoverIndex) { return }

    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
    const clientOffset = monitor.getClientOffset()
    const hoverClientY = clientOffset.y - hoverBoundingRect.top

    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) { return }
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) { return }

    props.move(dragIndex, hoverIndex)
    monitor.getItem().no = hoverIndex
  }
}

const collectDrag = (connect, monitor) => {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

const collectDrop = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget()
  }
}

export class Chapter extends React.Component {
  constructor (props) {
    super(props)

    this._onClickRename = this._onClickRename.bind(this)
    this._onSaveRename = this._onSaveRename.bind(this)

    this._onClickDelete = this._onClickDelete.bind(this)
    this._onClickUnlock = this._onClickUnlock.bind(this)
    this._toggleDelete = this._toggleDelete.bind(this)
    this._toggleUnlock = this._toggleUnlock.bind(this)

    this._onClickAlignment = this._onClickAlignment.bind(this)
    this._isAdmin = this._isAdmin.bind(this)
    this._formatDate = this._formatDate.bind(this)

    this._onClickTitleDropdown = this._onClickTitleDropdown.bind(this)
    this._onClickCustomTitle = this._onClickCustomTitle.bind(this)
    this._toggleList = this._toggleList.bind(this)
    this._myHandler = this._myHandler.bind(this)
    this._viewOrEdit = this._viewOrEdit.bind(this)

    this.state = {
      isRenamingTitle: false,
      isRenameEmpty: false,
      showDeleteModal: false,
      showUnlockModal: false,
      open: false, // control if the dropdwon list is open or not
      canEdit: false
    }
  }

  _onClickAlignment (position) {
    const { book, chapter, update } = this.props
    if (!includes(['left', 'right'], position)) { return }

    function clickAlignment () {
      chapter.alignment[position] = !chapter.alignment[position]
      update(book, chapter)
    }

    return clickAlignment
  }

  _onClickRename () {
    this.setState({
      isRenamingTitle: true
    })
  }

  _onSaveRename (title) {
    // save button has been clicked from outside the component
    if (typeof title !== 'string') {
      // console.log(this.refs)
      title = this.refs.chapterInput.state.value
    }

    if (title.length === 0) {
      this.setState({
        isRenameEmpty: true
      })
      return
    }

    this.setState({
      isRenameEmpty: false
    })

    const { book, chapter, update } = this.props
    chapter.title = title

    update(book, chapter)

    this.setState({
      isRenamingTitle: false
    })
  }

  _onClickDelete () {
    const { chapter, remove } = this.props
    remove(chapter)
    this._toggleDelete()
  }

  _isAdmin () {
    const { roles } = this.props
    return includes(roles, 'admin')
  }

  _formatDate (timestamp) {
    const date = new Date(timestamp)

    const day = date.getDate()
    const month = date.getMonth() + 1
    const year = date.getFullYear()

    let hours = date.getHours().toString()
    if (hours.length === 1) {
      hours = '0' + hours
    }

    let minutes = date.getMinutes().toString()
    if (minutes.length === 1) {
      minutes = '0' + minutes
    }

    const theDate = month + '/' + day + '/' + year
    const theTime = hours + ':' + minutes
    const formatted = theDate + ' ' + theTime
    return formatted
  }

  _toggleDelete () {
    this.setState({ showDeleteModal: !this.state.showDeleteModal })
  }

  _toggleUnlock () {
    if (!this._isAdmin()) { return }
    this.setState({ showUnlockModal: !this.state.showUnlockModal })
  }

  _onClickUnlock () {
    const { book, chapter, update } = this.props
    const isAdmin = this._isAdmin()

    if (!isAdmin) { return }

    chapter.lock = null
    update(book, chapter)
    this._toggleUnlock()
  }

  _viewOrEdit () {
    const { roles, chapter } = this.props

    if (includes(roles, 'production-editor')) return this.setState({ canEdit: true })

    if (chapter.progress['review'] === 1 && includes(roles, 'author') ||
        chapter.progress['edit'] === 1 && includes(roles, 'copy-editor')) {
      this.setState({ canEdit: true })
    } else {
      this.setState({ canEdit: false })
    }
  }

  componentDidMount () {
    window.addEventListener('click', this._myHandler)
    this._viewOrEdit()
  }

  componentWillUnmount () {
    window.removeEventListener('click', this._myHandler)
  }

  _myHandler (evt) {
    if (evt.target.id === 'dropbutton' ||
        evt.target.parentElement.id === 'dropbutton' ||
        evt.target.classList.contains('caret')) {
      return
    }
    this.setState({
      open: false
    })
  }

  _onClickTitleDropdown (title) {
    const { book, chapter, update } = this.props
    const self = this

    if (title === '') {
      return
    }

    function clickTitleDropdown () {
      chapter.title = title
      update(book, chapter)
      setTimeout(() => {
        self.setState({open: false})
      }, 10)
    }

    return clickTitleDropdown
  }

  _onClickCustomTitle () {
    let customTitle = get(this.refs, 'dropDownInput.state.value', null)
    this._onClickTitleDropdown(customTitle)()
  }

  _toggleList () {
    this.setState({
      open: !this.state.open
    })
  }

  render () {
    const { book, chapter, type, title, connectDragSource, connectDropTarget, isDragging, update, roles, outerContainer } = this.props
    const { isRenamingTitle, isRenameEmpty } = this.state
    // const { _onSaveRename } = this

    const opacity = isDragging ? 0 : 1

    const align = chapter.alignment

    let titleArea = null
    let renameButton = null

    let renameEmptyError = isRenameEmpty
    ? (
      <span className={styles.emptyTitle}>
        New title cannot be empty
      </span>
    )
    : null

    if (type === 'chapter' || type === 'part') {
      // if type is chapter, make the title editable text
      let renameButtonText, renameButtonFunction

      const input = (
        <TextInput
          className="edit"
          ref="chapterInput"
          onSave={this._onSaveRename}
          value={title}
        />
      )

      if (isRenamingTitle) {
        titleArea = input
        renameButtonText = 'Save'
        renameButtonFunction = this._onSaveRename
      } else {
        titleArea = (<h3 onDoubleClick={this._onClickRename}> { title } </h3>)
        renameButtonText = 'Rename'
        renameButtonFunction = this._onClickRename
      }

      // add id so that it can be selected for testing
      // could do with refs, but that would mean mounting instead of
      // shallow rendering to access enzyme's refs() api method
      renameButton = (
        <a id="bb-rename"
          onClick={renameButtonFunction}>
          { renameButtonText } &nbsp;&nbsp;
        </a>
      )
    } else if (type === 'component') {
      // if type is component, make title a dropdown choice

      let dropdownOptions
      if (chapter.division === 'front') {
        dropdownOptions = [
          'Table of Contents',
          'Introduction',
          'Preface',
          'Preface 1',
          'Preface 2',
          'Preface 3',
          'Preface 4',
          'Preface 5',
          'Preface 6',
          'Preface 7',
          'Preface 8',
          'Preface 9',
          'Preface 10'
        ]
      } else if (chapter.division === 'back') {
        dropdownOptions = [
          'Appendix A',
          'Appendix B',
          'Appendix C'
        ]
      }

      const onClickTitleDropdown = this._onClickTitleDropdown

      let width = 180
      let TotalColumns = 1
      if (dropdownOptions.length > 9) {
        TotalColumns = Math.ceil(dropdownOptions.length / 5)
      }

      const menuItems = map(dropdownOptions, function (item, i) {
        const onClickItem = onClickTitleDropdown(item)

        return (
          <MenuItem
            className={styles.menuItem}
            onClick={onClickItem}
            key={i}>
            { item }
          </MenuItem>
        )
      })

      let columns = menuItems

      if (TotalColumns > 1) {
        columns = []
        let loopIt = 1

        while (loopIt <= width) {
          let start = (loopIt - 1) * 5
          let end = start + 5
          columns.push(slice(menuItems, start, end))
          loopIt += 1
        }
        columns = map(columns, function (column, i) {
          return (
            <div className={styles.menuItemContainer} key={i}>
              { column }
            </div>
          )
        })
      }

      width = (width * TotalColumns)

      titleArea = (
        <DropdownButton
          title={title}
          id="dropbutton"
          className={styles.dropDown}
          open={this.state.open}
          onClick={this._toggleList}
        >
          <div style={{ width: width }}>
            <div className={styles.dropDownInputContairer}>
              <TextInput
                ref="dropDownInput"
                className={styles.dropDownInput}
                onSave={this._onClickCustomTitle}
                placeholder="Type a custom title"
              />
            </div>

            { columns }
          </div>

        </DropdownButton>
      )
    }

    const editOrView = this.state.canEdit ? 'Edit' : 'View'

    const buttons = (
      <div>
        { renameButton }
        <LinkContainer
          to={`/manage/books/${book.id}/fragments/${chapter.id}`}
          id="bb-edit"
        >
          <a>{ editOrView } &nbsp;&nbsp;</a>
        </LinkContainer>

        <a id="bb-delete"
          onClick={this._toggleDelete}>
          Delete
        </a>
      </div>
    )

    let editorArea
    if (get(chapter, 'lock.editor.username')) {
      let message = ' is editing'
      if (chapter.lock.timestamp && this._isAdmin()) {
        message = ' has been editing since ' + this._formatDate(chapter.lock.timestamp)
      }

      editorArea = (
        <a id="bb-unlock"
          className={styles.lEditing}
          onClick={this._toggleUnlock}>

          <i
            className={styles.lockIcon + ' fa fa-lock'}
            aria-hidden="true"
            alt="unlock"
          />
          <span className={styles.lockMessage}>
            { chapter.lock.editor.username + message}
          </span>

        </a>
      )
    }

    const rightArea = chapter.lock ? editorArea : buttons

    const clickAlignmentLeft = this._onClickAlignment('left')
    const clickAlignmentRight = this._onClickAlignment('right')

    return connectDragSource(connectDropTarget(
      <li
        className={styles.chapterContainer + ' col-lg-12 bb-chapter ' + (chapter.subCategory === 'chapter' ? styles.isChapter : styles.isPart)}
        style={{ opacity: opacity }}>
        <div className={styles.grabIcon + ' ' + (chapter.division === 'body' ? styles.grabIconBody : '')}>
          <i className="fa fa-circle" />
          <div className={styles.tooltip}>
            Grab to sort
          </div>
        </div>

        <div className={styles.chapterMainContent}>
          <div className={styles.chapterTitle}>
            { titleArea }
            { renameEmptyError }
            <div className={styles.separator} />
          </div>

          <div className={styles.chapterActions + ' pull-right'}>
            {rightArea}
          </div>

          <div className={styles.chapterBottomLine} />

          <div className={styles.secondLineContainer}>
            <div className={styles.noPadding + ' col-lg-2 col-md-12 col-sm-12 col-xs-12'}>
              <div id="bb-upload" className={styles.btnFile}>
                Upload Word
                <input
                  type="file"
                  accept=".docx"
                  title=" "
                />
              </div>
            </div>

            <ul className={styles.secondActions + ' col-lg-7 col-md-12 col-sm-12 col-xs-12'}>
              <ProgressIndicator
                type="style"
                book={book}
                chapter={chapter}
                update={update}
                roles={roles}
                outerContainer={outerContainer}
                hasIcon
                viewOrEdit={this._viewOrEdit}
              />

              <ProgressIndicator
                type="edit"
                book={book}
                chapter={chapter}
                update={update}
                roles={roles}
                outerContainer={outerContainer}
                hasIcon
                viewOrEdit={this._viewOrEdit}
              />

              <ProgressIndicator
                type="review"
                book={book}
                chapter={chapter}
                update={update}
                roles={roles}
                outerContainer={outerContainer}
                hasIcon
                viewOrEdit={this._viewOrEdit}
              />

              <ProgressIndicator
                type="clean"
                book={book}
                chapter={chapter}
                roles={roles}
                outerContainer={outerContainer}
                update={update}
                viewOrEdit={this._viewOrEdit}
              />
            </ul>

            <div className={styles.noPadding + ' col-lg-3 col-md-12 col-sm-12 col-xs-12'}>
              <ul className={styles.pagePosition}>
                <li>left &nbsp;</li>

                <li onClick={clickAlignmentLeft}>
                  <div className={styles.leftRightBox + ' ' + styles.leftBox}>
                    <div className={align.left ? styles.boxActive : styles.boxInactiveHover} />
                  </div>
                </li>

                <li onClick={clickAlignmentRight}>
                  <div className={styles.leftRightBox + ' ' + styles.rightBox}>
                    <div className={align.right ? styles.boxActive : styles.boxInactiveHover} />
                  </div>
                </li>

                <li>
                  <div className={styles.boxDiver} />
                </li>

                <li>&nbsp; right</li>
              </ul>
            </div>

            <div className={styles.separator} />
          </div>
        </div>

        <BookBuilderModal
          title={'Delete ' + type}
          chapter={chapter}
          action="delete"
          successText="Delete"
          type={type}
          successAction={this._onClickDelete}
          show={this.state.showDeleteModal}
          toggle={this._toggleDelete}
          container={outerContainer}
        />

        <BookBuilderModal
          title={'Unlock ' + type}
          chapter={chapter}
          action="unlock"
          successText="Unlock"
          type={type}
          successAction={this._onClickUnlock}
          show={this.state.showUnlockModal}
          toggle={this._toggleUnlock}
          container={outerContainer}
        />

        <div className={chapter.division === 'body' ? styles.leftBorderBody : styles.leftBorderComponent} />
      </li>
    ))
  }
}

Chapter.propTypes = {
  chapter: React.PropTypes.object.isRequired,
  book: React.PropTypes.object.isRequired,

  remove: React.PropTypes.func.isRequired,
  update: React.PropTypes.func.isRequired,
  type: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired,
  outerContainer: React.PropTypes.object.isRequired,
  // roles: React.PropTypes.array.isRequired,
  roles: React.PropTypes.array,

  // react-dnd
  connectDragSource: React.PropTypes.func.isRequired,
  connectDropTarget: React.PropTypes.func.isRequired,
  isDragging: React.PropTypes.bool.isRequired
}

// combine them, as each chapter can be both a source and a target
export default flow(
  DragSource(itemTypes.CHAPTER, chapterSource, collectDrag),
  DropTarget(itemTypes.CHAPTER, chapterTarget, collectDrop)
)(Chapter)
