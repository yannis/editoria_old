'use strict'

// Substance packages
var BasePackage = require('substance/packages/base/BasePackage')
var ParagraphPackage = require('substance/packages/paragraph/ParagraphPackage')
var HeadingPackage = require('substance/packages/heading/HeadingPackage')
var CodeblockPackage = require('substance/packages/codeblock/CodeblockPackage')
var BlockquotePackage = require('substance/packages/blockquote/BlockquotePackage')
var ListPackage = require('substance/packages/list/ListPackage')
var EmphasisPackage = require('substance/packages/emphasis/EmphasisPackage')
var StrongPackage = require('substance/packages/strong/StrongPackage')
var CodePackage = require('substance/packages/code/CodePackage')
var SubscriptPackage = require('substance/packages/subscript/SubscriptPackage')
var SuperscriptPackage = require('substance/packages/superscript/SuperscriptPackage')
var PersistancePackage = require('substance/packages/persistence/PersistencePackage')
var ProseArticle = require('substance/packages/prose-editor/ProseArticle')
var ProseEditorToolbar = require('substance/packages/prose-editor/ProseEditorToolbar')

// My Elements
var LinkPackage = require('./elements/link/LinkPackage')
var CommentPackage = require('./elements/comment/CommentPackage')
var DialoguePackage = require('./elements/dialogue/DialoguePackage')
var SourceNotePackage = require('./elements/source_note/SourceNotePackage')
var ExtractPackage = require('./elements/extract/ExtractPackage')
var NumberedListPackage = require('./elements/numbered_list/NumberedListPackage')
var NotePackage = require('./elements/note/NotePackage')
var NoStyleListPackage = require('./elements/no_style_list/NoStyleListPackage')

// My Overlay
var Overlay = require('./Overlay')

module.exports = {
  name: 'simple-editor',
  configure: function (config, options) {
    config.defineSchema({
      name: 'prose-article',
      ArticleClass: ProseArticle,
      defaultTextType: 'paragraph'
    })
    config.setToolbarClass(ProseEditorToolbar)

    // Now import base packages
    config.import(BasePackage, {
      noBaseStyles: options.noBaseStyles
    })

    config.import(ParagraphPackage)
    config.import(HeadingPackage)
    config.import(CodeblockPackage)
    config.import(BlockquotePackage)
    config.import(ListPackage)
    config.import(EmphasisPackage)
    config.import(StrongPackage)
    config.import(SubscriptPackage)
    config.import(SuperscriptPackage)
    config.import(CodePackage)
    config.import(LinkPackage)
    config.import(PersistancePackage)

    config.import(DialoguePackage)
    config.import(ExtractPackage)
    config.import(NoStyleListPackage)
    config.import(NotePackage)
    config.import(NumberedListPackage)
    config.import(SourceNotePackage)
    config.import(CommentPackage)

    config.addComponent('overlay', Overlay)
  }
}
