'use strict'

var SubstanceContainerEditor = require('substance/ui/ContainerEditor')
var Surface = require('substance/ui/Surface')
var uuid = require('substance/util/uuid')

function ContainerEditor () {
  ContainerEditor.super.apply(this, arguments)
}

ContainerEditor.Prototype = function () {
  this.render = function ($$) {
    var el = Surface.prototype.render.call(this, $$)

    var doc = this.getDocument()
    var containerId = this.getContainerId()
    var containerNode = doc.get(containerId)

    if (!containerNode) {
      console.warn('No container node found for ', containerId)
    }

    el.addClass('sc-container-editor container-node ' + containerId)
      .attr({
        spellCheck: false,
        'data-id': containerId
      })

    // if it IS empty, handle in didMount
    if (!this.isEmpty()) {
      containerNode.getNodes().forEach(function (node) {
        el.append(this._renderNode($$, node).ref(node.id))
      }.bind(this))
    }

    if (!this.props.disabled) {
      el.addClass('sm-enabled')
      el.setAttribute('contenteditable', true)
    }

    return el
  }

  this.didMount = function () {
    Surface.prototype.didMount.apply(this, arguments)
    this.container.on('nodes:changed', this.onContainerChange, this)
    if (this.isEmpty()) this.createText()
  }

  // create an empty paragraph with an empty node
  // then select it for cursor focus
  this.createText = function () {
    var newSel

    this.transaction(function (tx) {
      var container = tx.get(this.props.containerId)
      var textType = tx.getSchema().getDefaultTextType()

      var node = tx.create({
        id: uuid(textType),
        type: textType,
        content: ''
      })

      container.show(node.id)

      newSel = tx.createSelection({
        type: 'property',
        path: [ node.id, 'content' ],
        startOffset: 0,
        endOffset: 0
      })
    }.bind(this))

    this.rerender()
    this.setSelection(newSel)
  }
}

SubstanceContainerEditor.extend(ContainerEditor)

module.exports = ContainerEditor
