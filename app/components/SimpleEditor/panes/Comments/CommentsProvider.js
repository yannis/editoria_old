import _ from 'lodash'

var createAnnotation = require('substance/model/transform/createAnnotation')
var deleteNode = require('substance/model/transform/deleteNode')
var TocProvider = require('substance/ui/TOCProvider')

function CommentsProvider () {
  CommentsProvider.super.apply(this, arguments)
  this.activeEntry = null
}

CommentsProvider.Prototype = function () {
  this.computeEntries = function () {
    var comments = this.getComments()
    var entries = this.sortNodes(comments)

    return entries
  }

  this.reComputeEntries = function () {
    this.entries = this.computeEntries()
    this.emit('comments:updated')
  }

  this.resolveComment = function (surface, id) {
    var self = this
    var ds = this.config.documentSession
    var doc = ds.getDocument()

    var commentNode = doc.get(id)

    var path = commentNode.path
    var startOffset = commentNode.startOffset
    var endOffset = commentNode.endOffset

    var sel = ds.createSelection(path, startOffset, endOffset)

    var resolvedNodeData = {
      selection: sel,
      node: {
        type: 'resolved-comment'
      }
    }

    // create resolved comment annotation on the same selection the comment was on
    ds.transaction(function (tx, args) {
      var annotation = createAnnotation(doc, resolvedNodeData)
      var resolvedCommentId = annotation.node.id
      self.markCommentAsResolved(id, resolvedCommentId)
    })

    // and remove existing comment
    this.deleteCommentNode(surface, id)

    this.reComputeEntries()
  }

  this.removeEmptyComments = function (surface) {
    var self = this

    var comments = self.getComments()
    var commentsContent = self.config.comments

    _.each(comments, function (value, key) {
      if (commentsContent && !commentsContent[key]) {
        setTimeout(function () {
          self.deleteCommentNode(surface, key)
        }, 0)
      }
    })
  }

  this.deleteCommentNode = function (surface, id) {
    surface.transaction(function (tx, args) {
      deleteNode(tx, { nodeId: id })
    })
  }

  // When a comment gets resolved the comment on the fragment gets a resolved property.
  // The resolved property is the id of the new resolved comment annotation.
  // If the user does not save the document after resolving, the id for the comment is still there, so it will display correctly.
  // If the user resolves the comment, does not save and then resolves the same comment again, the resolve property will simply be overwritten.
  // If the document is saved, the comment id will not exist in the document any more, so it will not be rendered.
  // If the now resolved comment needs to be unresolved, the original contents can easily be found by looking for the object with the resolved comment's id in the resolved property.
  this.markCommentAsResolved = function (commentId, resolvedId) {
    var comments = this.config.comments
    var update = this.config.updateComments
    var id = commentId

    if (!comments[id]) return
    comments[id].resolved = resolvedId
    update(comments)
  }

  this.getComments = function () {
    var doc = this.getDocument()
    var nodes = doc.getNodes()

    // get all notes from the document
    var comments = _.pickBy(nodes, function (value, key) {
      return value.type === 'comment'
    })

    return comments
  }

  // TODO -- do I need to override this?
  this.handleDocumentChange = function (change) {
    var doc = this.getDocument()
    var needsUpdate = false
    var tocTypes = this.constructor.static.tocTypes

    for (var i = 0; i < change.ops.length; i++) {
      var op = change.ops[i]
      var nodeType
      if (op.isCreate() || op.isDelete()) {
        var nodeData = op.getValue()
        nodeType = nodeData.type
        if (_.includes(tocTypes, nodeType)) {
          needsUpdate = true
          break
        }
      } else {
        var id = op.path[0]
        var node = doc.get(id)
        if (node && _.includes(tocTypes, node.type)) {
          needsUpdate = true
          break
        }
      }
    }

    if (needsUpdate) {
      // need a timeout here, to make sure that the updated doc has rendered
      // the annotations, so that the comment box list can be aligned with them
      var self = this
      setTimeout(function () {
        self.reComputeEntries()
      })
    }
  }

  this.sortNodes = function (nodes) {
    var comments = _.clone(nodes)
    var doc = this.getDocument()
    var container = doc.get('body')

    // sort notes by
    //   the index of the containing block
    //   their position within that block

    comments = _.map(comments, function (comment) {
      var blockId = comment.path[0]
      var blockPosition = container.getPosition(blockId)
      var nodePosition = comment.startOffset

      return {
        id: comment.id,
        blockPosition: blockPosition,
        nodePosition: nodePosition,
        node: comment
      }
    })

    return _.sortBy(comments, ['blockPosition', 'nodePosition'])
  }

  this.getActiveEntry = function () {
    return this.activeEntry
  }

  this.setActiveEntry = function (id) {
    this.activeEntry = id
    this.emit('comments:updated')
  }

  this.removeActiveEntry = function () {
    this.activeEntry = null
    this.emit('comments:updated')
  }

  this.focusTextArea = function (id) {
    setTimeout(function () {
      var textarea = document.getElementById(id)
      if (textarea) {
        textarea.focus()
      }
    }, 0)
  }
}

TocProvider.extend(CommentsProvider)

CommentsProvider.static.tocTypes = ['comment']

module.exports = CommentsProvider
