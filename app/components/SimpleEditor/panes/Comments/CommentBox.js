'use strict'

var Component = require('substance/ui/Component')
var Icon = require('substance/ui/FontAwesomeIcon')

// var CommentBoxList = require('./CommentBoxList')
var CommentList = require('./CommentList')

function CommentBox () {
  CommentBox.super.apply(this, arguments)
  this.inputHeight = 0
}

CommentBox.Prototype = function () {
  // TODO -- fix class names
  this.render = function ($$) {
    var active = this.props.active
    var comments = this.props.comments.data
    var entry = this.props.entry

    if (!active) { comments = comments.slice(0, 1) } // preview
    var commentList = $$(CommentList, {
      active: active,
      box: this,
      comments: comments
    })

    var box = $$('li')
      .attr('data-comment', entry.id)
      .addClass('sc-comment-pane-item')
      .addClass('animation')
      .append(commentList)
      .on('click', this.makeActive)

    if (active) {
      if (comments.length > 0) {
        var resolveIcon = $$(Icon, { icon: 'fa-check' })

        var resolve = $$('button')
          .append(resolveIcon)
          .attr('title', 'Resolve')
          .addClass('comment-resolve')
          .on('click', this.resolve)
      }

      var textarea = $$('textarea')
        .attr('rows', '1')
        .ref('commentReply')
        .attr('id', entry.id)
        .on('keydown', this.onKeyDown)
        .on('input', this.textAreaAdjust)

      var replyIcon = $$(Icon, { icon: 'fa-plus' })

      // TODO -- refactor classes!!!
      var reply = $$('button')
        .attr('disabled', true)
        .attr('title', 'Reply')
        .addClass('comment-reply')
        .append(replyIcon)
        .ref('commentReplyButton')
        .on('click', this.reply)

      var inputBox = $$('div')
        .append(textarea, reply, resolve)

      box.addClass('sc-comment-active')
        .append(inputBox)
    }

    return box
  }

  this.textAreaAdjust = function (event) {
    var value = this.refs.commentReply.getValue()
    var replyBtn = this.refs.commentReplyButton

    if (value.trim().length > 0) {
      replyBtn.removeAttr('disabled')
    } else {
      replyBtn.attr('disabled', 'true')
    }

    var parent = this.props.parent
    var textArea = event.path[0]

    textArea.style.height = '1px'
    textArea.style.height = (textArea.scrollHeight) + 'px'
    parent.setTops()
  }

  this.reply = function () {
    var comments = this.props.comments
    var id = this.props.entry.id
    var provider = this.getProvider()
    var textarea = this.refs.commentReply
    var text = textarea.getValue()
    var user = this.props.user.username

    comments.data.push({
      user: user,
      text: text
    })

    this.update(comments)

    textarea.setValue('')
    this.rerenderBoxList()
    provider.focusTextArea(id)
  }

  this.update = function (comments) {
    var id = this.props.entry.id
    var parent = this.props.parent

    parent.update(id, comments)
  }

  this.makeActive = function () {
    var id = this.props.entry.id
    var provider = this.getProvider()

    var activeEntry = provider.getActiveEntry()
    if (activeEntry === id) return

    provider.setActiveEntry(id)
    provider.focusTextArea(id)
  }

  this.resolve = function () {
    var id = this.props.entry.id
    var provider = this.getProvider()

    var surfaceManager = this.context.surfaceManager
    // TODO -- get container id dynamically
    var body = surfaceManager.getSurface('body')

    provider.resolveComment(body, id)
  }

  this.getProvider = function () {
    return this.context.commentsProvider
  }

  // sends the event to trigger a rerender of the whole box list
  // cannot simply rerender this box, as its height might have changed
  // and the boxes underneath might need to move
  this.rerenderBoxList = function () {
    var provider = this.getProvider()
    provider.emit('comments:updated')
  }

  this.onKeyDown = function (e) {
    if (e.keyCode === 27) {
      var provider = this.getProvider()
      var surface = this.context.surfaceManager.getSurface('body')

      provider.removeActiveEntry()
      provider.removeEmptyComments(surface)
    }

    if (e.keyCode === 13) {
      var textAreaValue = this.refs.commentReply.getValue()
      if (textAreaValue.trim().length > 0) {
        this.reply()
      }
    }
  }
}

Component.extend(CommentBox)

module.exports = CommentBox
