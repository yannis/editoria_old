'use strict'

var Icon = require('substance/ui/FontAwesomeIcon')
var Component = require('substance/ui/Component')

function Comment () {
  Comment.super.apply(this, arguments)
}

Comment.Prototype = function () {
  this.render = function ($$) {
    var active = this.props.active
    var name = this.props.user
    var text = this.props.text

    // preview
    if (!active) {
      if (text.length > 100) {
        text = text.substring(0, 100) + '...'
      }

      var resolveIcon = $$(Icon, { icon: 'fa-check' })
      var iconCircle = $$('div').addClass('sc-comment-resolve-icon-circle')
        .append(resolveIcon)

      var resolveIconEl = $$('div')
        .addClass('sc-comment-resolve-icon')
        .attr('title', 'Resolve comment')
        .append(iconCircle)
        .on('click', this.resolve)
    }

    var nameEl = $$('span').addClass('comment-user-name').append(name, ' - ')
    var textEl = $$('span').addClass('comment-text').append(text)

    var entry = $$('div')
      .addClass('sc-comment-entry')
      .append(
        nameEl,
        textEl
      )

    return $$('div').addClass('single-comment-row')
      .append(
        entry,
        resolveIconEl
      )
  }

  this.resolve = function (e) {
    e.stopPropagation()
    var box = this.props.box
    box.resolve()
  }
}

Component.extend(Comment)

module.exports = Comment
