'use strict'

var Component = require('substance/ui/Component')

var Comment = require('./Comment')

function CommentList () {
  CommentList.super.apply(this, arguments)
}

CommentList.Prototype = function () {
  this.render = function ($$) {
    var active = this.props.active
    var box = this.props.box
    var comments = this.props.comments

    var commentsEl = comments.map(function (comment) {
      return $$(Comment, {
        active: active,
        box: box,
        text: comment.text,
        user: comment.user
      })
    })

    return $$('div')
      .addClass('comment-list')
      .append(commentsEl)
  }
}

Component.extend(CommentList)

module.exports = CommentList
