'use strict'

import _ from 'lodash'

var Component = require('substance/ui/Component')

var CommentBox = require('./CommentBox')

function CommentBoxList () {
  CommentBoxList.super.apply(this, arguments)
  this.tops = []
}

CommentBoxList.Prototype = function () {
  this.didMount = function () {
    var provider = this.getProvider()
    provider.on('comments:updated', this.onCommentsUpdated, this)

    this.setTops()
  }

  this.didUpdate = function () {
    this.setTops()
  }

  this.render = function ($$) {
    var self = this

    var provider = self.getProvider()
    var entries = provider.getEntries()
    var activeEntry = provider.activeEntry

    var listItems = entries.map(function (entry, i) {
      var active = (entry.id === activeEntry)

      return $$(CommentBox, {
        active: active,
        comments: self.props.comments[entry.id] || { data: [] },
        entry: entry,
        parent: self,
        user: self.props.user
      })
    })

    if (listItems.length > 0) {
      this.send('showComments')
    } else {
      this.send('hideComments')
    }

    return $$('ul')
      .addClass('sc-comment-pane-list')
      .append(listItems)
  }

  this.update = function (id, newComments) {
    var comments = this.props.comments
    var update = this.props.update

    if (!comments[id]) comments[id] = { data: [] }
    comments[id] = newComments
    update(comments)
  }

  this.setTops = function () {
    var provider = this.getProvider()
    var entries = provider.getEntries()
    var activeEntry = provider.activeEntry
    this.calculateTops(entries, activeEntry)
  }

  this.calculateTops = function (entries, active) {
    // var activePos
    var result = []
    var boxes = []

    _.each(entries, function (entry, pos) {
      // initialize annotationTop and boxHeight, as there is a chance that
      // annotation and box elements are not found by the selector
      // (as in when creating a new comment)
      var annotationTop = 0
      var boxHeight = 0
      var top = 0

      var isActive = false
      if (entry.id === active) isActive = true

      // get position of annotation in editor
      var annotationEl = document.querySelector('span[data-id="' + entry.id + '"]')
      if (annotationEl) annotationTop = parseInt(annotationEl.offsetTop)

      // get height of this comment box
      var boxEl = document.querySelector('li[data-comment="' + entry.id + '"]')
      if (boxEl) boxHeight = parseInt(boxEl.offsetHeight)

      // keep the elements to add the tops to at the end
      boxes.push(boxEl)

      // where the box should move to
      top = annotationTop

      // if the above comment box has already taken up the height, move down
      if (pos > 0) {
        var previousBox = entries[pos - 1]
        var previousEndHeight = previousBox.endHeight
        if (annotationTop < previousEndHeight) {
          top = previousEndHeight + 2
        }
      }

      // store where the box ends to be aware of overlaps in the next box
      entry.endHeight = top + boxHeight + 2
      result[pos] = top

      // if active, move as many boxes above as needed to bring it to the annotation's height
      if (isActive) {
        // activePos = pos
        entry.endHeight = annotationTop + boxHeight + 2
        result[pos] = annotationTop

        var b = true
        var i = pos

        // first one active, none above
        if (i === 0) b = false

        while (b) {
          var boxAbove = entries[i - 1]
          var boxAboveEnds = boxAbove.endHeight
          var currentTop = result[i]

          var doesOverlap = boxAboveEnds > currentTop
          if (doesOverlap) {
            var overlap = boxAboveEnds - currentTop
            result[i - 1] -= overlap
          }

          if (!doesOverlap) b = false
          if (i <= 1) b = false
          i -= 1
        }
      }
    })

    // var previousTops = this.tops
    // this.tops = result

    // give each box the correct top
    _.each(boxes, function (box, i) {
      var val = result[i] + 'px'
      box.style.top = val

      // var previousVal = previousTops[i] + 'px'
      // if (previousVal < entries[activePos].endHeight) {
      //   previousVal = entries[activePos].endHeight
      // }
      //
      // box.style.top = previousVal

      // setTimeout(function () {
      //   box.style.top = val
      // }, 0)

      // TODO better name!!!
      // box.classList.add('animation')
    })
  }

  this.getProvider = function () {
    return this.context.commentsProvider
  }

  this.onCommentsUpdated = function () {
    this.rerender()
  }

  this.dispose = function () {
    var provider = this.getProvider()
    provider.off(this)
  }
}

Component.extend(CommentBoxList)

module.exports = CommentBoxList
