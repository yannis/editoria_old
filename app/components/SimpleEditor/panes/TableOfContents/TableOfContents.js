'use strict'

// import React from 'react'
// import { LinkContainer } from 'react-router-bootstrap'

var Icon = require('substance/ui/FontAwesomeIcon')
var Toc = require('substance/ui/TOC')

function TableOfContents () {
  TableOfContents.super.apply(this, arguments)

  // TODO is this ever called?
  this.handleAction('tocEntrySelected', function (nodeId) {
    var editor = this.context.controller
    editor.scrollTo(nodeId)

    var provider = this.context.tocProvider
    provider.activeEntry = nodeId

    this.rerender()
  })

  this.render = function ($$) {
    var book = this.props.book
    var fragment = this.props.fragment

    var tocProvider = this.context.tocProvider
    var activeEntry = tocProvider.activeEntry

    var latinLevel = {
      '1': 'I',
      '2': 'II',
      '3': 'III'
    }

    var tocEntries = $$('div')
      .addClass('se-toc-entries')
      .ref('tocEntries')

    var entries = tocProvider.getEntries()
    for (var i = 0; i < entries.length; i++) {
      var entry = entries[i]
      var level = entry.level

      var tocEntryEl = $$('a')
        .addClass('se-toc-entry')
        .addClass('sm-level-' + level)
        .attr({
          href: '#',
          'data-id': entry.id
        })
        .ref(entry.id)
        .on('click', this.handleClick)
        .append(
          $$(Icon, {icon: 'fa-caret-right'}),
          latinLevel[entry.level] + '.  ',
          entry.name
        )
      if (activeEntry === entry.id) {
        tocEntryEl.addClass('sm-active')
      }
      tocEntries.append(tocEntryEl)
    }

    var location = window.location
    var bookBuilderUrl = location.origin + '/manage/books/' + book.id + '/book-builder'
    var bookLink = $$('a')
      .attr({
        title: 'Back to book builder for ' + book.title,
        href: bookBuilderUrl
      })
      .append('Book: ' + book.title)

    var info = $$('div')
      .addClass('sc-toc-panel-info')
      .append(
        bookLink,
        $$('br'),
        'Chapter name: ' + fragment.title,
        $$('hr')
      )

    var el = $$('div')
      .addClass('sc-toc-panel')
      .ref('panelEl')
      .append(
        info,
        tocEntries
      )

    var headerElement = $$('div')
      .addClass('sc-toc-panel-header')
      .append('Chapter Structure')

    var tocContent = $$('div')
      .addClass('sc-toc-wrapper')
      .append(
        headerElement,
        el
      )

    return $$('div')
      .addClass('sc-toc-container')
      .append(tocContent)
  }
}

// TableOfContents.Prototype = function () {}

Toc.extend(TableOfContents)

module.exports = TableOfContents
