import _ from 'lodash'

var TocProvider = require('substance/ui/TOCProvider')

function NotesProvider () {
  NotesProvider.super.apply(this, arguments)
}

NotesProvider.Prototype = function () {
  this.computeEntries = function () {
    var doc = this.getDocument()
    var nodes = doc.getNodes()

    // get all notes from the document
    var notes = _.pickBy(nodes, function (value, key) {
      return value.type === 'note'
    })

    var entries = this.sortNodes(notes)
    return entries
  }

  this.sortNodes = function (nodes) {
    var notes = _.clone(nodes)
    var doc = this.getDocument()
    var container = doc.get('body')

    // sort notes by
    //   the index of the containing block
    //   their position within that block

    notes = _.map(notes, function (note) {
      var blockId = note.path[0]
      var blockPosition = container.getPosition(blockId)
      var nodePosition = note.startOffset

      return {
        id: note.id,
        content: note['note-content'],
        blockPosition: blockPosition,
        nodePosition: nodePosition,
        node: note
      }
    })

    return _.sortBy(notes, ['blockPosition', 'nodePosition'])
  }
}

TocProvider.extend(NotesProvider)

NotesProvider.static.tocTypes = ['note']

module.exports = NotesProvider
