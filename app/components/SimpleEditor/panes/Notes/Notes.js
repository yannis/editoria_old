'use strict'

var Component = require('substance/ui/Component')

function Notes () {
  Component.apply(this, arguments)
}

Notes.Prototype = function () {
  // use toc:updated to avoid rewriting TOCProvider's this.handleDocumentChange
  this.didMount = function () {
    var provider = this.getProvider()
    provider.on('toc:updated', this.onNotesUpdated, this)
  }

  this.render = function ($$) {
    var provider = this.getProvider()
    var entries = provider.getEntries()

    var listItems = entries.map(function (entry, i) {
      return $$('li')
        .attr('data-id', entry.id)
        .addClass('sc-notes-footer-item')
        .append(entry.content)
    })
    if (listItems.length === 0) return $$('div')

    return $$('ol')
        .addClass('sc-notes-footer')
        .append(listItems)
  }

  this.getProvider = function () {
    return this.context.notesProvider
  }

  this.onNotesUpdated = function () {
    this.rerender()
  }

  this.dispose = function () {
    var provider = this.getProvider()
    provider.off(this)
  }
}

Component.extend(Notes)

module.exports = Notes
