'use strict'

var Extract = require('./Extract')
var ExtractComponent = require('./ExtractComponent')
var ExtractHTMLConverter = require('./ExtractHTMLConverter')

module.exports = {
  name: 'extract',
  configure: function (config) {
    config.addNode(Extract)
    config.addComponent(Extract.static.name, ExtractComponent)
    config.addConverter('html', ExtractHTMLConverter)

    config.addTextType({
      name: 'extract',
      data: {type: 'extract'}
    })

    config.addLabel('extract', {
      en: 'Extract'
    })
  }
}
