'use strict'

var TextBlock = require('substance/model/TextBlock')

function Extract () {
  Extract.super.apply(this, arguments)
}

TextBlock.extend(Extract)

Extract.static.name = 'extract'

module.exports = Extract
