'use strict'

var TextBlockComponent = require('substance/ui/TextBlockComponent')
// var SourceNote = require('../source_note/SourceNote.js')

function ExtractComponent () {
  ExtractComponent.super.apply(this, arguments)
}

ExtractComponent.Prototype = function () {
  this.didMount = function () {
    this.addClass('sc-extract')
  }
}

TextBlockComponent.extend(ExtractComponent)

module.exports = ExtractComponent
