'use strict'

var breakList = require('substance/packages/list/breakList')

module.exports = {
  register: function (behavior) {
    behavior.defineBreak('numbered-list-item', breakList)
  }
}
