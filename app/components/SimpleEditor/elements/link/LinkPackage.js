'use strict'

var Link = require('substance/packages/link/Link')
var LinkComponent = require('substance/packages/link/LinkComponent')
var LinkCommand = require('substance/packages/link/LinkCommand')
var LinkHTMLConverter = require('substance/packages/link/LinkHTMLConverter')
var LinkTool = require('substance/packages/link/LinkTool')

module.exports = {
  name: 'link',
  configure: function (config) {
    config.addNode(Link)

    config.addComponent(Link.static.name, LinkComponent)
    config.addConverter('html', LinkHTMLConverter)
    config.addCommand(LinkCommand)
    config.addTool(LinkTool)

    config.addIcon(LinkCommand.static.name, { 'fontawesome': 'fa-link' })
    config.addIcon('open-link', { 'fontawesome': 'fa-external-link' })

    config.addLabel('link', {
      en: 'Link'
    })
  }
}
