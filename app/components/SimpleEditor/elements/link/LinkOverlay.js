'use strict'

var EditLinkTool = require('substance/packages/link/EditLinkTool')
var Prompt = require('substance/ui/Prompt')

function LinkOverlay () {
  LinkOverlay.super.apply(this, arguments)
}

LinkOverlay.Prototype = function () {
  this.render = function ($$) {
    var el = $$('div').addClass('sc-edit-link-tool')

    var commandStates = this.context.commandManager.getCommandStates()
    var isLink = commandStates.link.active
    if (!isLink) return el

    var node = commandStates.link.node
    var doc = node.getDocument()
    var urlPath = this.getUrlPath(node.id)

    el.append(
      $$(Prompt).append(
        $$(Prompt.Input, {
          type: 'url',
          path: urlPath,
          placeholder: 'Paste or type a link url'
        }),
        $$(Prompt.Separator),
        $$(Prompt.Link, {
          name: 'open-link',
          href: doc.get(urlPath),
          title: this.getLabel('open-link')
        }),
        $$(Prompt.Action, {name: 'delete', title: this.getLabel('delete')})
          .on('click', this.onDelete)
      )
    )

    return el
  }

  this.getUrlPath = function (id) {
    var propPath = this.constructor.static.urlPropertyPath
    return [id].concat(propPath)
  }

  this.onDelete = function (e) {
    e.preventDefault()
    var surface = this.context.surfaceManager.getFocusedSurface()

    var commandStates = this.context.commandManager.getCommandStates()
    var node = commandStates.link.node

    surface.transaction(function (tx, args) {
      tx.delete(node.id)
      return args
    })
  }
}

EditLinkTool.extend(LinkOverlay)

module.exports = LinkOverlay
