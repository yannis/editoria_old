'use strict'

var PropertyAnnotation = require('substance/model/PropertyAnnotation')

function Comment () {
  Comment.super.apply(this, arguments)
}

PropertyAnnotation.extend(Comment)

Comment.static.name = 'comment'

module.exports = Comment
