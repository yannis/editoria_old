'use strict'

var Component = require('substance/ui/Component')
var createAnnotation = require('substance/model/transform/createAnnotation')
var documentHelpers = require('substance/model/documentHelpers')
var Icon = require('substance/ui/FontAwesomeIcon')

function CommentBubble () {
  CommentBubble.super.apply(this, arguments)
}

CommentBubble.Prototype = function () {
  this.render = function ($$) {
    var commandStates = this.context.commandManager.getCommandStates()
    var commentState = commandStates.comment

    var title = ''
    // var title = 'Create a new comment'

    // /////////////////////////////////////////////////////////
    // SET ACTIVE COMMENT //////////////////////////////////////

    var commentsProvider = this.context.commentsProvider
    var activeComment = commentsProvider.getActiveEntry()
    var isComment = commandStates['comment'].active

    if (isComment) {
      var selectionState = this.context.documentSession.getSelectionState()
      var commentAnnotations = selectionState.getAnnotationsForType('comment')
      // TODO what happens with multiple comments on the same cursor position?
      var comment = commentAnnotations[0]

      if (activeComment !== comment.id) {
        commentsProvider.setActiveEntry(comment.id)
      }
    } else {
      if (activeComment) {
        // HACK -- pass the surface, as I cannot get it from the editor on start
        var surface = this.context.surfaceManager.getFocusedSurface()
        commentsProvider.removeEmptyComments(surface)
        commentsProvider.removeActiveEntry()
      }
    }
    // /////////////////////////////////////////////////////////

    var icon = $$(Icon, { icon: 'fa-comment' })
      .addClass('sc-comment-icon')

    if (commentState.mode !== 'create') return $$('div')

    return $$('div')
      .attr('title', title)
      .addClass('circle')
      .on('click', this.setCommentState)
      .append(icon)
  }

  this.getActiveComment = function () {
    var session = this.context.documentSession
    var sel = session.getSelection()
    var comment = documentHelpers.getPropertyAnnotationsForSelection(
      session.getDocument(),
      sel,
      { type: 'comment' }
    )

    return comment[0]
  }

  this.setCommentState = function () {
    var commandStates = this.context.commandManager.getCommandStates()
    var session = this.context.documentSession
    var surface = this.context.surfaceManager.getFocusedSurface()

    var sel = session.getSelection()

    if (commandStates['comment'].mode === 'create') {
      surface.transaction(function (tx, args) {
        createAnnotation(tx, {
          selection: sel,
          node: {
            type: 'comment'
          }
        })
      })
    }
  }
}

Component.extend(CommentBubble)

module.exports = CommentBubble
