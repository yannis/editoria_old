'use strict'

var Comment = require('./Comment')
var CommentCommand = require('./CommentCommand')
var CommentComponent = require('./CommentComponent')

var ResolvedCommentPackage = require('./ResolvedCommentPackage')

module.exports = {
  name: 'comment',
  configure: function (config) {
    config.import(ResolvedCommentPackage)

    config.addNode(Comment)
    config.addComponent(Comment.static.name, CommentComponent)
    config.addCommand(CommentCommand)

    config.addIcon('comment', { 'fontawesome': 'fa-comment' })

    config.addLabel('comment', {
      en: 'Comment'
    })
  }
}
