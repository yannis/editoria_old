'use strict'

var AnnotationComponent = require('substance/ui/AnnotationComponent')

function CommentComponent () {
  CommentComponent.super.apply(this, arguments)
}

CommentComponent.Prototype = function () {
  this.render = function ($$) {
    var el = $$('span')
      .attr('data-id', this.props.node.id)
      .addClass(this.getClassNames())

    if (this.props.node.highlighted) {
      el.addClass('sm-highlighted')
    }

    el.append(this.props.children)
    el.on('click', this.focus)

    return el
  }

  this.focus = function () {
    var id = this.props.node.id
    var provider = this.context.commentsProvider

    provider.focusTextArea(id)
  }
}

AnnotationComponent.extend(CommentComponent)

module.exports = CommentComponent
