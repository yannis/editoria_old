'use strict'

var AnnotationCommand = require('substance/ui/AnnotationCommand')

var ResolvedCommentCommand = AnnotationCommand.extend()

ResolvedCommentCommand.static.name = 'resolved-comment'

module.exports = ResolvedCommentCommand
