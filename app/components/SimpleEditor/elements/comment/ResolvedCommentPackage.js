'use strict'

// var Comment = require('./Comment')
// var CommentCommand = require('./CommentCommand')
// var CommentComponent = require('./CommentComponent')

var ResolvedComment = require('./ResolvedComment')
var ResolvedCommentCommand = require('./ResolvedCommentCommand')

module.exports = {
  name: 'resolved-comment',
  configure: function (config) {
    config.addNode(ResolvedComment)
    config.addCommand(ResolvedCommentCommand)

    // config.addComponent(Comment.static.name, CommentComponent)

    // config.addIcon('comment', { 'fontawesome': 'fa-comment' })
    //
    // config.addLabel('comment', {
    //   en: 'Comment'
    // })
  }
}
