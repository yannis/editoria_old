'use strict'

var PropertyAnnotation = require('substance/model/PropertyAnnotation')

function ResolvedComment () {
  ResolvedComment.super.apply(this, arguments)
}

PropertyAnnotation.extend(ResolvedComment)

ResolvedComment.static.name = 'resolved-comment'

module.exports = ResolvedComment
