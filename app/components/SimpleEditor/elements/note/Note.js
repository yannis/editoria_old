'use strict'

var InlineNode = require('substance/model/InlineNode')

function Note () {
  Note.super.apply(this, arguments)
}

InlineNode.extend(Note)

Note.static.name = 'note'

Note.static.defineSchema({
  'note-content': {
    type: 'string',
    default: ''
  }
})

module.exports = Note
