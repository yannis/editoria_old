'use strict'

var InlineNodeCommand = require('substance/ui/InlineNodeCommand')

function NoteCommand () {
  NoteCommand.super.apply(this, arguments)
}

NoteCommand.Prototype = function () {
  this.createNodeData = function () {
    return {
      type: 'note'
    }
  }
}

InlineNodeCommand.extend(NoteCommand)

NoteCommand.static.name = 'note'

module.exports = NoteCommand
