'use strict'

var Component = require('substance/ui/Component')

function TextArea () {
  Component.apply(this, arguments)
}

TextArea.Prototype = function () {
  this.render = function ($$) {
    var documentSession = this.context.documentSession
    var doc = documentSession.getDocument()
    var val = doc.get(this.props.path)

    var el = $$('textarea')
      .attr({
        rows: this.props.rows || '1',
        cols: this.props.columns || '40',
        placeholder: this.props.placeholder || 'Type your text here'
      })
      .addClass('se-note-textarea')
      .append(val)
      .on('keyup', this.textAreaAdjust)

    return el
  }

  this.textAreaAdjust = function (event) {
    var textArea = event.path[0]
    textArea.style.height = '1px'
    textArea.style.height = (textArea.scrollHeight) + 'px'
  }
}

Component.extend(TextArea)

module.exports = TextArea
