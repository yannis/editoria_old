'use strict'

var Note = require('./Note')
var NoteComponent = require('./NoteComponent')
var NoteTool = require('./NoteTool')
var NoteCommand = require('./NoteCommand')
var EditNoteTool = require('./EditNoteTool')

// var NoteFooter = require('./NoteFooter')
// var NoteFooterComponent = require('./NoteFooterComponent')

module.exports = {
  name: 'note',
  configure: function (config) {
    config.addNode(Note)
    config.addComponent(Note.static.name, NoteComponent)
    config.addCommand(NoteCommand)

    config.addTool(NoteTool)

    config.addIcon('note', { 'fontawesome': 'fa-bookmark' })
    // config.addIcon('instagram', { 'fontawesome': 'fa-instagram' })

    // config.addNode(NoteFooter)
    // config.addComponent(NoteFooter.static.name, NoteFooterComponent)

    config.addLabel('note', {
      en: 'Note'
    })

    config.addTool(EditNoteTool, { overlay: true })
  }
}
