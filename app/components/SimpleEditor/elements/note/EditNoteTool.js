'use strict'

import { clone } from 'lodash'

var Component = require('substance/ui/Component')
var Prompt = require('substance/ui/Prompt')

var PromptTextArea = require('./PromptTextArea')

function EditNoteTool () {
  EditNoteTool.super.apply(this, arguments)
}

EditNoteTool.Prototype = function () {
  this.saveNote = function (event) {
    var selected = this.getSelection()
    var noteContent = this.el.find('textarea').val()
    var documentSession = this.context.documentSession

    documentSession.transaction(function (tx, args) {
      var path = [selected.node.id, 'note-content']
      tx.set(path, noteContent)
    })
  }

  this.render = function ($$) {
    var el = $$('div').addClass('sc-edit-note-tool')

    var selected = this.getSelection()
    if (!selected.node) return el

    // TODO -- on this.getLabel add a label to package and call it save note

    el.append(
      $$(Prompt).append(
        $$(PromptTextArea, {
          path: [selected.node.id, 'note-content'],
          placeholder: 'Type your note here'
        }),
        $$(Prompt.Separator),
        $$(Prompt.Action, { name: 'save' })
          .on('click', this.saveNote)
      )
    )

    this.setTextAreaHeight() // to properly adjust text area height depending on text

    return el
  }

  this.getSelection = function () {
    var surface = this.context.surfaceManager.getFocusedSurface()
    if (!surface) return {}
    var commandStates = this.context.commandManager.getCommandStates()

    if (commandStates['note'].active) {
      var note = commandStates['note']
      commandStates['note'].disabled = true // disable note so you can't click it when already active
      return {
        node: note.node
      }
    } else {
      return {
        node: null
      }
    }
  }

  this.setTextAreaHeight = function () {
    setTimeout(function () {
      var textarea = document.querySelector('.se-note-textarea')
      textarea.style.height = (textarea.scrollHeight) + 'px'
    }, 0)
  }
}

Component.extend(EditNoteTool)

EditNoteTool.static.getProps = function (commandStates) {
  if (commandStates['note'].active) {
    return clone(commandStates['note'])
  } else {
    return undefined
  }
}

EditNoteTool.static.name = 'edit-note'

module.exports = EditNoteTool
