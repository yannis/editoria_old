'use strict'

var DocumentNode = require('substance/model/DocumentNode')

function NoteFooter () {
  NoteFooter.super.apply(this, arguments)
}

DocumentNode.extend(NoteFooter)

NoteFooter.static.name = 'note-footer'

// NoteFooter.static.defineSchema({
//   name: 'text',
//   description: 'text'
// })

module.exports = NoteFooter
