'use strict'

var AnnotationTool = require('substance/ui/AnnotationTool')

function NoteTool () {
  NoteTool.super.apply(this, arguments)
}

AnnotationTool.extend(NoteTool)

NoteTool.static.name = 'note'

module.exports = NoteTool
