'use strict'

var Component = require('substance/ui/Component')

function Note () {
  Note.super.apply(this, arguments)
}

Note.Prototype = function () {
  this.render = function ($$) {
    var el = $$('note')
      .attr('note-content', this.props.node['note-content'])
      .addClass('sc-note')
      // .append('')

    return el
  }

  this.dispose = function () {
    this.props.node.off(this)
  }
}

Component.extend(Note)

module.exports = Note
