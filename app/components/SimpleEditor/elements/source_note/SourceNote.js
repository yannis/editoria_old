'use strict'

var PropertyAnnotation = require('substance/model/PropertyAnnotation')

function SourceNote () {
  SourceNote.super.apply(this, arguments)
}

PropertyAnnotation.extend(SourceNote)

SourceNote.static.name = 'source-note'

module.exports = SourceNote
