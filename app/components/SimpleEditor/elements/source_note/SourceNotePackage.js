'use strict'

var SourceNote = require('./SourceNote')
var SourceNoteTool = require('./SourceNoteTool')
var SourceNoteCommand = require('./SourceNoteCommand')

module.exports = {
  name: 'source-note',
  configure: function (config) {
    config.addNode(SourceNote)
    config.addCommand(SourceNoteCommand)
    config.addTool(SourceNoteTool)
    config.addIcon('source-note', { 'fontawesome': 'fa-book' })
    // config.addStyle(__dirname, '_sourceNote.scss')

    config.addLabel('source-note', {
      en: 'Source Note'
    })
  }
}
