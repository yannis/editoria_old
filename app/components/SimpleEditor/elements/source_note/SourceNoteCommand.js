'use strict'

var AnnotationCommand = require('substance/ui/AnnotationCommand')

var SourceNoteCommand = AnnotationCommand.extend()

SourceNoteCommand.static.name = 'source-note'

module.exports = SourceNoteCommand
