'use strict'

var AnnotationTool = require('substance/ui/AnnotationTool')

function SourceNoteTool () {
  AnnotationTool.apply(this, arguments)
}

AnnotationTool.extend(SourceNoteTool)

SourceNoteTool.static.name = 'source-note'

module.exports = SourceNoteTool
