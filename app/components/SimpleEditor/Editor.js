'use strict'

// import _ from 'lodash'

var ProseEditor = require('substance/packages/prose-editor/ProseEditor')
var ScrollPane = require('substance/ui/ScrollPane')
var SplitPane = require('substance/ui/SplitPane')
// var TabbedPane = require('substance/ui/TabbedPane')
var TocProvider = require('substance/ui/TOCProvider')

var Comments = require('./panes/Comments/CommentBoxList')
var CommentsProvider = require('./panes/Comments/CommentsProvider')
var ContainerEditor = require('./ContainerEditor')
var Notes = require('./panes/Notes/Notes')
var NotesProvider = require('./panes/Notes/NotesProvider')
var Overlay = require('./Overlay')
var TableOfContents = require('./panes/TableOfContents/TableOfContents')

function Editor () {
  Editor.super.apply(this, arguments)

  this.handleActions({
    'showComments': function () { this.toggleCommentsArea(true) },
    'hideComments': function () { this.toggleCommentsArea(false) }
  })
}

Editor.Prototype = function () {
  this.willUpdateState = function () {}

  this.didMount = function () {
    this.documentSession.on('didUpdate', this._documentSessionUpdated, this)
    this.extendState({ editorReady: true })
  }

  this.render = function ($$) {
    var el = $$('div').addClass('sc-prose-editor')

    // left side: editor and toolbar
    var toolbar = this._renderToolbar($$)
    var editor = this._renderEditor($$)
    var footerNotes = $$(Notes)
    var props = {
      book: this.props.book,
      fragment: this.props.fragment
    }

    var toc = $$(TableOfContents, props)

    var editorWithFooter = $$('div')
      .append(
        editor,
        footerNotes
      )

    var commentsPane = this.state.editorReady
    ? $$(Comments, {
      comments: this.props.fragment.comments || {},
      update: this.props.updateComments,
      user: this.props.user
    }).addClass('sc-comments-pane')
    : $$('div')

    var editorWithComments = $$(SplitPane, { sizeA: '100%', splitType: 'vertical' })
      .append(
        editorWithFooter,
        commentsPane
      )

    var contentPanel = $$(ScrollPane, {
      scrollbarType: 'substance',
      scrollbarPosition: 'right',
      overlay: Overlay
    })
    .append(editorWithComments)
    .attr('id', 'content-panel')
    .ref('contentPanel')

    var contentPanelWithSplitPane = $$(SplitPane, { sizeA: '75%', splitType: 'vertical' })
        .append(
          contentPanel,
          toc
        )

    el.append(
          $$(SplitPane, { splitType: 'horizontal' })
            .append(toolbar, contentPanelWithSplitPane)
        )

    // wrap both sides into one split pane

    return el
  }

  this._renderEditor = function ($$) {
    var configurator = this.props.configurator
    return $$(ContainerEditor, {
      disabled: this.props.disabled,
      documentSession: this.documentSession,
      commands: configurator.getSurfaceCommandNames(),
      containerId: 'body',
      textTypes: configurator.getTextTypes()
    }).ref('body')
  }

  this.getInitialState = function () {
    return {
      editorReady: false
    }
  }

  this.scrollTo = function (nodeId) {
    this.refs.contentPanel.scrollTo(nodeId)
  }

  this.toggleCommentsArea = function (show) {
    var self = this

    setTimeout(function () {
      var el = self.refs.contentPanel

      if (show && !el.hasClass('sc-has-comments')) {
        el.addClass('sc-has-comments')
      }

      if (!show && el.hasClass('sc-has-comments')) {
        el.removeClass('sc-has-comments')
      }
    }, 0)
  }

  this.getChildContext = function () {
    var doc = this.doc
    var _super = Editor.super.prototype
    var oldContext = _super.getChildContext.apply(this, arguments)

    // toc provider
    var tocConfig = { 'containerId': 'body' }
    var tocProvider = new TocProvider(doc, tocConfig)

    // notes provider
    var notesProvider = new NotesProvider(doc)

    // comments provider
    var commentsProvider = new CommentsProvider(doc, {
      comments: this.props.fragment.comments,
      documentSession: this.documentSession,
      toggleCommentsArea: this.toggleCommentsArea,
      updateComments: this.props.updateComments
    })

    // attach all to context
    return { ...oldContext,
      tocProvider: tocProvider,
      notesProvider: notesProvider,
      commentsProvider: commentsProvider
    }
  }
}

ProseEditor.extend(Editor)

module.exports = Editor
