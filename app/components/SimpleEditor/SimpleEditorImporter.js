'use strict'

var HTMLImporter = require('substance/model/HTMLImporter')
var SimpleArticle = require('./SimpleArticle')

var converters = [
  require('substance/packages/paragraph/ParagraphHTMLConverter'),
  require('substance/packages/heading/HeadingHTMLConverter'),
  require('substance/packages/codeblock/CodeblockHTMLConverter'),
  require('substance/packages/blockquote/BlockquoteHTMLConverter'),
  require('substance/packages/strong/StrongHTMLConverter'),
  require('substance/packages/emphasis/EmphasisHTMLConverter'),
  require('substance/packages/link/LinkHTMLConverter'),
  require('substance/packages/subscript/SubscriptHTMLConverter'),
  require('substance/packages/superscript/SuperscriptHTMLConverter'),
  require('substance/packages/code/CodeHTMLConverter'),

  require('./elements/source_note/SourceNoteHTMLConverter'),
  require('./elements/extract/ExtractHTMLConverter'),
  require('./elements/note/NoteHTMLConverter'),
  require('./elements/comment/CommentHTMLConverter'),
  require('./elements/comment/ResolvedCommentHTMLConverter')
]

function SimpleImporter (config) {
  SimpleImporter.super.call(this, {
    schema: config.schema,
    converters: converters,
    DocumentClass: SimpleArticle
  })
}

SimpleImporter.Prototype = function () {
  /*
    Takes an HTML string.
  */
  this.convertDocument = function (bodyEls) {
    // Just to make sure we always get an array of elements
    if (!bodyEls.length) bodyEls = [bodyEls]
    this.convertContainer(bodyEls, 'body')
  }
}

HTMLImporter.extend(SimpleImporter)

SimpleImporter.converters = converters

module.exports = SimpleImporter
