import { find } from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as Actions from 'pubsweet-core/app/actions'
// import { fragmentsOfCollection } from 'pubsweet-core/app/helpers/Utils'

import SimpleEditor from './SimpleEditor'

export class SimpleEditorWrapper extends React.Component {
  constructor (props) {
    super(props)

    this.save = this.save.bind(this)
    this.updateComments = this.updateComments.bind(this)
    this._checkRights = this._checkRights.bind(this)

    this.state = {
      canEdit: false
    }
  }

  componentWillMount () {
    const { getCollections, getFragments } = this.props.actions
    getCollections().then(result => {
      getFragments(result.collections[0])
    })
    this._checkRights()
  }

  // componentDidUpdate () {
  //   this._checkRights()
  // }

  _checkRights () {
    const { fragment, user } = this.props

    if ((fragment.progress['review'] === 1 && user.teams[0].teamType.name === 'Author') ||
        (fragment.progress['edit'] === 1 && user.teams[0].teamType.name === 'Copy Editor')) {
      this.setState({ canEdit: true })
    } else {
      this.setState({ canEdit: false })
    }
    if (user.teams[0].teamType.name === 'Production Editor') {
      this.setState({ canEdit: true })
    }
  }

  save (source, callback) {
    const { book, fragment } = this.props
    const { updateFragment } = this.props.actions

    fragment.source = source
    updateFragment(book, fragment)
  }

  updateComments (newComments) {
    const { book, fragment } = this.props
    const { updateFragment } = this.props.actions

    fragment.comments = newComments
    updateFragment(book, fragment)
  }

  render () {
    const { book, fragment, user } = this.props

    return (
      <SimpleEditor
        book={book}
        canEdit={this.state.canEdit}
        fragment={fragment}
        onSave={this.save}
        updateComments={this.updateComments}
        user={user}
      />
    )
  }
}

SimpleEditorWrapper.propTypes = {
  book: React.PropTypes.object.isRequired,
  fragment: React.PropTypes.object.isRequired,
  actions: React.PropTypes.object.isRequired,
  user: React.PropTypes.object.isRequired
}

function mapStateToProps (state, ownProps) {
  const bookId = ownProps.params.bookId
  let book = find(state.collections, function (c) {
    return c.id === bookId
  })

  const fragmentId = ownProps.params.fragmentId
  let fragment = state.fragments[fragmentId]

  return {
    fragment: fragment,
    book: book,
    user: state.currentUser.user
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SimpleEditorWrapper)
