'use strict'

var HTMLExporter = require('substance/model/HTMLExporter')
var ProseArticle = require('substance/packages/prose-editor/ProseArticle')
var SimpleArticle = require('./SimpleArticle')
var schema = ProseArticle.schema

var converters = [
  require('substance/packages/paragraph/ParagraphHTMLConverter'),
  require('substance/packages/heading/HeadingHTMLConverter'),
  require('substance/packages/codeblock/CodeblockHTMLConverter'),
  require('substance/packages/blockquote/BlockquoteHTMLConverter'),
  require('substance/packages/strong/StrongHTMLConverter'),
  require('substance/packages/emphasis/EmphasisHTMLConverter'),
  require('substance/packages/link/LinkHTMLConverter'),
  require('substance/packages/subscript/SubscriptHTMLConverter'),
  require('substance/packages/superscript/SuperscriptHTMLConverter'),
  require('substance/packages/code/CodeHTMLConverter'),

  require('./elements/source_note/SourceNoteHTMLConverter'),
  require('./elements/extract/ExtractHTMLConverter'),
  require('./elements/note/NoteHTMLConverter'),
  require('./elements/comment/CommentHTMLConverter'),
  require('./elements/comment/ResolvedCommentHTMLConverter')
]

function SimpleExporter () {
  SimpleExporter.super.call(this, {
    schema: schema,
    converters: converters,
    DocumentClass: SimpleArticle
  })
}

SimpleExporter.Prototype = function () {
  this.convertDocument = function (doc, htmlEl) {
    // TODO delete second arg
    var elements = this.convertContainer(doc, this.state.containerId)
    var out = elements.map(function (el) {
      // console.log('element', el)
      // console.log('outer html', el.outerHTML)
      return el.outerHTML
    })
    return out.join('')
  }

  this.convertContainer = function (container) {
    // console.log('convert container')
    if (!container) {
      throw new Error('Illegal arguments: container is mandatory.')
    }

    var doc = container.getDocument()
    this.state.doc = doc
    var elements = []

    container.data.nodes.body.nodes.forEach(function (id) {
      var node = doc.get(id)
      // console.log('node', node)
      var nodeEl = this.convertNode(node)
      elements.push(nodeEl)
    }.bind(this))
    return elements
  }
}

HTMLExporter.extend(SimpleExporter)

SimpleExporter.converters = converters

module.exports = SimpleExporter
