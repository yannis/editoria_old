'use strict'

var SubstanceOverlay = require('substance/ui/Overlay')

var CommentTooltip = require('./elements/comment/CommentBubble')
var NoteTooltip = require('./elements/note/EditNoteTool')
var LinkOverlay = require('./elements/link/LinkOverlay')

function Overlay () {
  Overlay.super.apply(this, arguments)

  this.left = 0
  this.top = 0

  this.bubble = {
    left: 0,
    top: 0
  }
}

Overlay.Prototype = function () {
  this.render = function ($$) {
    var top = this.top + 'px'

    var NoteTooltipEl = $$(NoteTooltip)
      .css('left', this.left.note)
      .css('top', top)

    var CommentTooltipEl = $$(CommentTooltip)
      .css('left', this.bubble.left)
      .css('top', this.bubble.top)

    var LinkOverlayEl = $$(LinkOverlay)
      .css('left', this.left.link)
      .css('top', top)

    return $$('div')
      .addClass('sm-hidden')
      .ref('overlayContent')
      .append(
        NoteTooltipEl,
        CommentTooltipEl,
        LinkOverlayEl
      )
  }

  this.position = function (hints) {
    if (!hints) return

    var top = hints.rectangle.top + hints.rectangle.height

    var link = document.querySelector('.sc-edit-link-tool')
    var note = document.querySelector('.sc-edit-note-tool')

    var left = {
      link: this.calculateLeft(link, hints),
      note: this.calculateLeft(note, hints)
    }

    this.left = left
    this.top = top
    this.bubble = this.getBubblePosition()

    this.rerender()
    this.el.removeClass('sm-hidden')
  }

  this.calculateLeft = function (el, hints) {
    var contentWidth = el.offsetWidth
    var left = hints.rectangle.left + hints.rectangle.width / 2 - contentWidth / 2

    left = Math.max(left, 0)
    // console.log(contentWidth)
    var maxLeftPos = hints.rectangle.left + hints.rectangle.width + hints.rectangle.right - contentWidth - 60
    left = Math.min(left, maxLeftPos)

    return left + 'px'
  }

  this.getBubblePosition = function () {
    var body = this.context.surfaceManager.getFocusedSurface()
    if (!body) return
    var containerWidth = body.el.htmlProp('offsetWidth')

    // get the computed percentage padding of the content container
    var contentComputedStyles = window.getComputedStyle(document.querySelector('.se-content'))
    var contentMarginRight = Number(contentComputedStyles['padding-right'].slice(0, -2))

    var bubblePosition = {
      top: (this.top - 30) + 'px',
      left: (containerWidth + contentMarginRight + 20) + 'px'
    }

    return bubblePosition
  }
}

SubstanceOverlay.extend(Overlay)

module.exports = Overlay
