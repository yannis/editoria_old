import React from 'react'
import ReactDOM from 'react-dom'
import { Alert } from 'react-bootstrap'

var Component = require('substance/ui/Component')
var Configurator = require('substance/packages/prose-editor/ProseEditorConfigurator')
var DocumentSession = require('substance/model/DocumentSession')

var Editor = require('./Editor')
var Importer = require('./SimpleEditorImporter')
var SimpleExporter = require('./SimpleEditorExporter')

import config from './config.js'
// import fixture from './fixture'
import './SimpleEditor.scss'

export default class SimpleEditor extends React.Component {
  constructor (props) {
    super(props)
    this.save = this.save.bind(this)
  }

  createSession () {
    var configurator = new Configurator().import(config)
    configurator.setSaveHandler({
      saveDocument: this.save
    })
    configurator.addImporter('prose-article', Importer)
    const { fragment } = this.props
    let source
    if (fragment) { source = fragment.source }

    var importer = configurator.createImporter('prose-article')
    var doc = importer.importDocument(source)
    var documentSession = new DocumentSession(doc)

    return {
      documentSession: documentSession,
      configurator: configurator
    }
  }

  save (source, changes, callback) {
    const { onSave } = this.props
    var exporter = new SimpleExporter()
    onSave(exporter.exportDocument(source), callback)
  }

  componentDidMount () {
    const { canEdit } = this.props
    var el = ReactDOM.findDOMNode(this)

    var session = this.createSession()
    var documentSession = session.documentSession
    var configurator = session.configurator
    this.writer = Component.mount(Editor, {
      book: this.props.book,
      configurator: configurator,
      containerId: 'body',
      disabled: !canEdit, // set to true read only mode
      documentSession: documentSession,
      fragment: this.props.fragment,
      updateComments: this.props.updateComments,
      user: this.props.user
    }, el)
  }

  // New props arrived, update the editor
  // componentDidUpdate () {
  //   console.log('did update')
  //
  //   var session = this.createSession()
  //   var documentSession = session.documentSession
  //   var configurator = session.configurator
  //
  //   this.writer.extendProps({
  //     documentSession: documentSession,
  //     configurator: configurator
  //   })
  //
  //   console.log(this.writer)
  // }
  //
  // componentWillUnmount () {
  //   this.writer.dispose()
  // }

  render () {
    const { canEdit } = this.props
    let viewMode = !canEdit
    ? (
      <Alert bsStyle="warning" className="view-mode">
        Editor is in View Mode
      </Alert>
    )
    : null

    return (
      <div className="editor-wrapper">
        {viewMode}
      </div>
    )
  }
}

SimpleEditor.propTypes = {
  book: React.PropTypes.object.isRequired,
  canEdit: React.PropTypes.bool,
  fragment: React.PropTypes.object.isRequired,
  onSave: React.PropTypes.func.isRequired,
  updateComments: React.PropTypes.func.isRequired,
  user: React.PropTypes.object.isRequired
}
