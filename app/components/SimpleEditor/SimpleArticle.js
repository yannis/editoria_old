'use strict'

var ProseArticle = require('substance/packages/prose-editor/ProseArticle')

function SimpleArticle (schema) {
  ProseArticle.call(this, schema)
}

SimpleArticle.Prototype = function () {
  this.getDocument = function () {
    return this
  }
}

ProseArticle.extend(SimpleArticle)

module.exports = SimpleArticle
